# Projet Infra B1

Projet Infrastructure - Bachelor 1 Ynov

## Installer la machine

Pour installer la machine, il suffit de suivre les étapes dans le fichier [VM-install.md](./VM-install.md)

- Installation de la machine
    - 1. Création de la VM
    - 2. Installation de Java (OpenJDK)
    - 3. Installation de l'interface web
        - A. Installation du serveur web (nginx)
        - B. Installation du panel

## Installer un serveur

Pour installer la machine, il suffit de suivre les étapes dans le fichier [MC-install.md](./MC-install.md)

- Installation du serveur
    - 1. Création du dossier serveur
    - 2. Écriture d'un fichier `eula.txt`
    - 3. Téléchargement du `server.jar`