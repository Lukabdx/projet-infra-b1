#!/usr/bin/env bash
# Création du dossier serveur
# -> Download du .jar (selon la version choisie)
# -> Création d'un fichier "eula.txt" avec "eula=true" dedans
# Config auto de base
# Lancement du .jar


echo -ne 'Installation en cours                                                                             (0%)\r'
#faire joli
dnf update -y 
#Update du system
echo -ne '###                                                                                               (3%)\r'
mkdir /srv/Server"$1"
mkdir /backup_serveur/Server"$1"
#creation dossier du server
sleep 0
echo -ne '########                                                                                          (10%)\r'
curl https://papermc.io/api/v2/projects/paper/versions/1.16.5/builds/794/downloads/paper-1.16.5-794.jar -o /srv/Server"$1"/Server.jar
#telechargement du fichier de lancement du serveur (server.jar)
echo "eula=true" > /srv/Server"$1"/eula.txt
#set eula file to true
sleep 0.1
echo -ne '#################                                                                                 (14%)\r'
echo "" > /etc/systemd/system/server"$1".service
#creation fichier service
echo "
[Unit]
Description=Lance backup.service à intervalles réguliers
Requires=backup""$1"".service

[Timer]
Unit=backup""$1"".service ""$1""
OnCalendar=hourly

[Install]
WantedBy=timers.target
" > /etc/systemd/system/backup"$1".timer
sudo curl https://gitlab.com/Lukabdx/projet-infra-b1/-/raw/main/scripts/backup.sh -o /etc/systemd/system/backup"$1".sh
echo "
[Unit]
Description=Auto server
[Service]
WorkingDirectory=/srv/Server""$1""
User=Server""$1""
Type=simple
ExecStart=/usr/bin/java -jar /srv/Server""$1""/Server.jar
[Install]
WantedBy=multi-user.target
" > /etc/systemd/system/server"$1".service
echo "
[Unit]
Description=Auto Backup

[Service]
ExecStart=/usr/bin/bash /etc/systemd/system/backup$1.sh ""$1""
Type=oneshot

[Install]
WantedBy=multi-user.target
" > /etc/systemd/system/backup"$1".service
#fichier service
sleep 0.2
echo -ne '##########################                                                                        (19%)\r'
useradd  Server"$1" -d /srv/Server"$1"
chown Server"$1" /srv/Server"$1"
#creation user et affection du folder aux users
sleep 0.3    
echo -ne '#################################                                                                 (24%)\r'
sleep 0.5
cat /etc/systemd/system/server"$1".service
#verif du fichier 
echo -ne '#####################################                                                             (35%)\r'
sleep 0.8
echo -ne '#############################################                                                     (39%)\r'
sleep 0.2 
echo -ne '#################################################                                                 (46%)\r'
sleep 1
echo -ne '######################################################                                            (55%)\r' 
sleep 0.2
echo -ne '############################################################                                      (64%)\r'
sleep 0.8
echo -ne '###################################################################                               (69%)\r'
echo "
#Minecraft server properties
#Thu Jan 13 15:13:15 CET 2022
spawn-protection=10
max-tick-time=60000
query.port=
generator-settings=3456789
sync-chunk-writes=true
force-gamemode=false
allow-nether=$
enforce-whitelist=false
gamemode=survival
broadcast-console-to-ops=true
enable-query=false
player-idle-timeout=0
text-filtering-config=
difficulty=normal
broadcast-rcon-to-ops=true
spawn-monsters=true
op-permission-level=4
pvp=true
entity-broadcast-range-percentage=100
snooper-enabled=true
level-type=default
enable-status=true
hardcore=false
enable-command-block=false
network-compression-threshold=256
max-players=""$5""
max-world-size=29999984
resource-pack-sha1=
function-permission-level=
rcon.port=""$3""
server-port=""$2""
debug=false
server-ip=137.74.195.202
spawn-npcs=true
allow-flight=false
level-name=world
view-distance=$
resource-pack=
spawn-animals=true
white-list=false
rcon.password=""$4""
generate-structures=true
online-mode=true
max-build-height=256
level-seed=3456789098765
prevent-proxy-connections=false
use-native-transport=true
enable-jmx-monitoring=false
motd=
rate-limit=0
enable-rcon=true
" > /srv/Server"$1"/server.properties
#edit server.properties | server.properties fichier de conf de minecraft server
#$2 = server port
#$3 = rcon port
#$4 =rcon password
#$5 = max players
sleep 0.3
echo -ne '#######################################################################                           (76%)\r'
sleep 0.3
###
echo -ne '###########################################################################                       (81%)\r'
sleep 0.7
echo -ne '#############################################################################                     (89%)\r'
sleep 1
echo -ne '################################################################################                  (92%)\r'
sleep 1.2
echo -ne '###################################################################################               (94%)\r'
sleep 1.5
echo -ne '#####################################################################################             (96%)\r'
systemctl enable server"$1" --now 
systemctl restart server"$1".service
systemctl daemon-reload
#activation du service a effect immediat 
#redemarrage du sercice
#actualisation des services
sleep 1.9
echo -ne '########################################################################################          (99%)\r'
sleep 3.3
echo -ne '###########################################################################################       (100%)\r'
echo -ne '\n'
journalctl -xe -u server"$1" -f