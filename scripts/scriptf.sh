#!/usr/bin/env bash
# script d'installation de la machine 
# Mise a jour de la
# Installation de Java de
# Download minecraft server.jar dans un dossier racine
# Setup Firewall Basique
# Création du service@ pere de lancement serveur.jar


echo -ne 'Setup en cours                                                                                    (0%)\r'
#c'est joli
dnf update -y
# mise ajour
echo -ne '###                                                                                               (3%)\r'
sleep 0
echo -ne '########                                                                                          (10%)\r'
dnf install java-1.8.0-openjdk java-1.8.0-openjdk-devel -y
#installation de java
sleep 0.1
echo -ne '#################                                                                                 (14%)\r'
dnf install epel-release -y
#Extra Packages for Enterprise Linux
sleep 0.2
echo -ne '##########################                                                                        (19%)\r'
dnf install httpd -y
#install du server apache
sleep 0.3    
echo -ne '#################################                                                                 (24%)\r'
systemctl enable httpd --now
#active avec effect immediat le serveur apache
sleep 0.5
echo -ne '#####################################                                                             (35%)\r'
mkdir /var/www/tp-linux-infra/
#cree le dossier du site web 
chown -R apache /var/www/tp-linux-infra/
#accorde toutes les permissions a l'utilisateur apache 
sleep 0.8
echo -ne '#############################################                                                     (39%)\r'
sleep 0.2 
echo -ne '#################################################                                                 (46%)\r'
mkdir /etc/httpd/sites-available
#creation dossier des site disponible
mkdir /etc/httpd/sites-enabled
#creation dossier des sites activé
sed -i 's/enforcing/permissive/g' /etc/selinux/config
sleep 1
echo -ne '######################################################                                            (55%)\r'
sudo chmod -R 755 /var/www/tp-linux-infra/
#authorisation de toute permission dans le dossier du serveur web 
echo "
<html>
    <head>
        <title>Welcome to Website!</title>
    </head>
    <body>
        <h1>Success! The virtual host is working! You did not mess it up thanks to Me</h1>
    </body>
</html>
" > /var/www/tp-linux-infra/index.html
#page index de tes
echo "
IncludeOptional sites-enabled/*.conf
" >> /etc/httpd/conf/httpd.conf
echo "
<VirtualHost *:80>
    ServerName tp-linux-infra.com
    ServerAlias www.tp-linux-infra.com
    DocumentRoot /var/www/tp-linux-infra
</VirtualHost>
" > /etc/httpd/sites-available/tp-linux-infra.conf
#fichier de configuration
sleep 0.2
echo -ne '############################################################                                      (64%)\r'
echo "
<Directory /var/www/tp-linux-infra/>
Options Indexes FollowSymLinks
AllowOverride None
Require all granted
</Directory>
" >> /etc/httpd/conf/httpd.conf
#ajout de la conf pour notre domain 
sleep 0.8
echo -ne '###################################################################                               (69%)\r'
sudo ln -s /etc/httpd/sites-available/example_domain.conf /etc/httpd/sites-enabled/
#syncronisation des fichiers
sleep 0.3
echo -ne '#######################################################################                           (76%)\r'
sudo systemctl restart httpd
#redemarre le service apache
sleep 0.3
echo -ne '###########################################################################                       (81%)\r'
sudo dnf install epel-release mod_ssl -y
#installation du module https
sleep 0.7
echo -ne '#############################################################################                     (89%)\r'
sudo dnf install python3-certbot-apache -y
#installation du certificateur apache
sleep 1
echo -ne '################################################################################                  (92%)\r'
sudo certbot --apache --agree-tos --redirect --hsts --staple-ocsp -d www.tp-linux-infra.com
#certification de notre domaine
dnf module reset php 
dnf module enable php:remi-8.1
#modification de la verison de php en 8.1
sleep 1.2
echo -ne '###################################################################################               (94%)\r'
dnf install php
php --version
#installation de la nouvelle version de php et check de la nouvelle version de php en 8.1
sleep 1.5
echo -ne '#####################################################################################             (96%)\r'
sleep 1.9
echo -ne '########################################################################################          (99%)\r'
sudo systemctl enable httpd --now
#active au demarrage et dans l'immediat le service httpd (apache)
sleep 3.3
echo -ne '###########################################################################################       (100%)\r'
echo -ne '\n'