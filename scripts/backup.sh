#!/usr/bin/env bash
#!Author Dreasy
filename="Server""$1""_$(date +"%y-%m-%d")_$(date +"%H-%M-%S").tar.gz"
cd /srv && tar -czf "/backup_serveur/Server""$1""/${filename}" "Server""$1"""
echo "Backup /backup_serveur/${filename} created successfully."
echo "[$(date +"%y:%m:%d") $(date +"%H:%M:%S")] Backup /srv/backup/${filename} created successfully." >> /var/log/backup/backup.log