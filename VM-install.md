# Installation de la machine

- [1. Création de la VM](#1-création-de-la-vm)
- [2. Installation de Java (OpenJDK)](#2-installation-de-java-openjdk)
- [3. Installation de l'interface web]()
    - [A. Installation de serveur web (nginx)]()
    - [B. Installation du panel]()

## 1. Création de la VM

Installer une VM c'est facile lol : ici on utilisera `Rocky Linux`.

**Prérequis sur la machine :**
- Un accès internet fonctionnel
- SELinux désactivé
    ```bash
    $ sudo sed -i 's/enforcing/permissive/g' /etc/selinux/config
    ```
- Une connexion SSH fonctionnelle
- Désactivation de SELinux
- Mise à jour de dnf
    ```bash
    $ sudo dnf update -y
    ```


## 2. Installation de Java (OpenJDK)

Pour permettre aux serveurs minecraft de tourner, il est nécessaire d'avoir Java (paquet `openjdk` version 8) d'installé sur la machine.

- Installation de OpenJDK v8
    ```bash
    $ sudo dnf install java-1.8.0-openjdk java-1.8.0-openjdk-devel -y
    ```
- Vérification que OpenJDK v8 est bien installé
    ```bash
    $ java -version
    ```

## 3. Installation du panel

Pour la gestion des serveurs (création, entretien et suppression) nous allons avoir besoin d'installer un panel. 

### A. Installation de serveur web (nginx)

Pour mettre en place le panel de gestion de serveurs, nous avons besoin d'un serveur web. Nous allons ici utiliser ``nginx`.

- Installation du paquet `nginx`
    ```bash
    $ sudo dnf install nginx -y
    ```
- Ouverture du port 80 (firewall)
    ```bash
    $ sudo firewall-cmd --add-port=80/tcp --permanent
    ```
